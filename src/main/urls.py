

from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as user_views

from cart.views import register_page, checkout

urlpatterns = [
    path("", include('cart.urls')),
    path('admin/', admin.site.urls),
    path("register/", register_page, name="register"),
    path("login/", user_views.LoginView.as_view(template_name="user/login.html"), name="login"),
    path("logout/", user_views.LogoutView.as_view(template_name="user/logout.html"), name="logout"),
]
