from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from django.core import validators


class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ["username", "email", "password1", "password2"]



class CreditCardForm(forms.Form):
    credit_card_number = forms.CharField(
        widget=forms.TextInput(attrs={
        "placeholder":"1234 1234 1234 1234"}
    ), validators=[validators.RegexValidator(r'^[1-9]+[0-9]*$')])
    month = forms.IntegerField(min_value=0, max_value=31)
    year = forms.IntegerField(min_value=2019)
    cvc = forms.IntegerField(min_value=10, max_value=9999)

